export { default as TheContactSection } from "./TheContactSection.vue";
export { default as ProductCard } from "./ProductCard.vue";
export { default as TheTitleSection } from "./TheTitleSection.vue";
export { default as TheProductsSection } from "./TheProductsSection.vue";
export { default as TheFooterSection } from "./TheFooterSection.vue";
